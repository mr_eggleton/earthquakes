var fs = require('fs');
var request = require('superagent');
var MagicCSV = require("magic-csv"); 
var csv = new MagicCSV({trim: true});
require("date-format-lite");


var mapbase = "http://open.mapquestapi.com/geocoding/v1/reverse?key=Fmjtd%7Cluu821u2nl%2Cbn%3Do5-94ax0r";
var sFileData = fs.readFileSync("data/6+earthquake-1900-2012.geojson.json").toString();
var bigquakes_usgs =   JSON.parse(sFileData);

var start = new Date ("1990/01/01");

sFileData = fs.readFileSync("data/deadly.json").toString();
var deadlyquakes_wikipedia =   JSON.parse(sFileData);

var deadly = deadlyquakes_wikipedia.results.filter(function(quake){
        var ddate = new Date(quake.date);
        return ddate > start;
});

 
csv.readFile("data/gdp_pcap.csv", function(err, stats) {
  var gdps = csv.getObjects(); // all objects
  var pretty =  gdps.map(function(line){
          return { "country":line["Country Name"],
              "1990": line["1990"] === "" ? 0 : parseInt(line["1990"], 10),
              "1991": line["1991"] === "" ? 0 : parseInt(line["1991"], 10),
              "1992": line["1992"] === "" ? 0 : parseInt(line["1992"], 10),
              "1993": line["1993"] === "" ? 0 : parseInt(line["1993"], 10),
              "1994": line["1994"] === "" ? 0 : parseInt(line["1994"], 10),
              "1995": line["1995"] === "" ? 0 : parseInt(line["1995"], 10),
              "1996": line["1996"] === "" ? 0 : parseInt(line["1996"], 10),
              "1997": line["1997"] === "" ? 0 : parseInt(line["1997"], 10),
              "1998": line["1998"] === "" ? 0 : parseInt(line["1998"], 10),
              "1999": line["1999"] === "" ? 0 : parseInt(line["1999"], 10),
              "2000": line["2000"] === "" ? 0 : parseInt(line["2000"], 10),
              "2001": line["2001"] === "" ? 0 : parseInt(line["2001"], 10),
              "2002": line["2002"] === "" ? 0 : parseInt(line["2002"], 10),
              "2003": line["2003"] === "" ? 0 : parseInt(line["2003"], 10),
              "2004": line["2004"] === "" ? 0 : parseInt(line["2004"], 10),
              "2005": line["2005"] === "" ? 0 : parseInt(line["2005"], 10),
              "2006": line["2006"] === "" ? 0 : parseInt(line["2006"], 10),
              "2007": line["2007"] === "" ? 0 : parseInt(line["2007"], 10),
              "2008": line["2008"] === "" ? 0 : parseInt(line["2008"], 10),
              "2009": line["2009"] === "" ? 0 : parseInt(line["2009"], 10),
              "2010": line["2010"] === "" ? 0 : parseInt(line["2010"], 10),
              "2011": line["2011"] === "" ? 0 : parseInt(line["2011"], 10),
              "2012": line["2010"] === "" ? 0 : parseInt(line["2010"], 10),
              "2013": line["2011"] === "" ? 0 : parseInt(line["2011"], 10),
              "2014": line["2010"] === "" ? 0 : parseInt(line["2010"], 10)
              
          };
  });
  
  fs.writeFileSync("data/gdpc.json", JSON.stringify(pretty));
  
  //console.log("gdps =", gdps);
  var countries = gdps.map(function(line){
      return line['Country Name'].toLowerCase();
  });
  //console.log("countries =", countries);
  var unknowns = [];
  var keys = [];
  var groups = [];
  
  var bigquakes = [];
  
  bigquakes_usgs.features.forEach(function(line, num) {
        var place = line.properties.place.split(",");
        //var rCountry = /(\s+|^)((east|north|south|west)(east|north|south|west)?(ern)?)(\s+|$)/gi;
        //var rWords = /(\s+|^)(near|the|coast|of|region|off)(\s+|$)/gi;
        var rCountry = /(^(\s*)(((east|north|south|west)(east|north|south|west)?(ern)?)|near|the|coast|of|off)\s)|(region$)/gi;
        var country = place[place.length - 1].replace(rCountry, " ").trim();
        var country_text = country;
        
        var found = true;
        if( countries.indexOf(country.toLowerCase()) === -1 ) {
            console.log("country missing =", country);
            found = false;
            country = false;
            var missing = {
                country: country,
                num: num,
                location: {latLng:{lat:line.geometry.coordinates[0],lng:line.geometry.coordinates[1]}}
                
            };
            unknowns.push(missing);
        } 
        
        var date = new Date(line.properties.time);
        var year = date.getFullYear();
        var date_string = date.format("YYYY/MM/DD");//""+year+"/"+date.getMonth()+"/"+date.getDate();
        var key = country_text+"_"+date_string+"_"+Math.round(line.properties.mag);
    
        var index = keys.indexOf(key);
        
        var newquake =  {
            country:        country,
            country_text:   country_text,
            mag:            line.properties.mag,
            time:           line.properties.time,
            year:           year,
            date:           date_string,
            url:            line.properties.url,
            notes:          "",
            deaths:         0
        };
        
        if(index === -1){
            index = keys.length;
            keys.push(key);
            bigquakes.push(newquake);    
        } else {
            if (newquake.mag > bigquakes[index].mag) {
                bigquakes[index] = newquake;
            }
        }
        
    });

    var iCalls = 0;
    /*
    unknowns.forEach(function(unknown){
        try {
            var loc = {location:unknown.location};
            iCalls ++;    
            request
              .get(mapbase)
              .query({ json: JSON.stringify(loc), num:unknown.num })
              .end(function(res){
                      iCalls --;
                      console.log("iCalls =", iCalls);
                      console.log("res.body.results[0].locations[0] =", res.body.results[0].locations[0]);
                      var country =  res.body.results[0].locations[0].adminArea1;
                      if(country) {
                          console.log("country =", country);
                          bigquakes[res.request.qs.num].country = country;
                      }
              });
        } catch (err){
            console.log(err);
        } 
    });
    */
    
    
    //console.log("unknowns =", unknowns);
    
    bigquakes = bigquakes.filter(function(quake){
            return quake.country; 
    });
    
    /*
    bigquakes = bigquakes.map(function(quake){
            var matches = deadly.filter(function(dquake){
                    //console.log("dquake.date =", dquake.date, "quake.date =", quake.date);
                    return dquake.date === quake.date;
                    
            });
            if(matches.length) {
                //console.log("quake=", quake, "matches", matches);
                console.log("matches =", matches.length);
                quake.deaths = matches[0].deaths;
                console.log("quake =", quake);
            }
            
    
    });
    */
    
    var iMatches = 0 ;
    deadly = deadly.map(function(dquake){
        var matches = bigquakes.filter(function(quake){
            //console.log("dquake.date =", dquake.date, "quake.date =", quake.date);
            return  dquake.date === quake.date && Math.round(dquake.mag) === Math.round(quake.mag);
        });
        
        if(matches.length) {
            //console.log("quake=", quake, "matches", matches);
            
            if(matches.length >  1) {
                
                var locmatches = matches.filter(function(locquake){
                    //console.log("dquake.date =", dquake.date, "quake.date =", quake.date);
                    return  dquake.location.toLowerCase() === locquake.country_text.toLowerCase();
            
                });
                
                if(locmatches.length === 1) {
                    matches = locmatches;
                
                }
                //console.log("dquake =", dquake);
                //console.log("matches =", matches);
            }
            
            matches[0].deaths = dquake.deaths;
            
            iMatches ++;
        }
    });
    
    console.log("iMatches =", iMatches);
    console.log("deadly =", deadly.length);
    console.log("bigquakes =", bigquakes.length);
    //console.log("with_country =", with_country.length);
    
    fs.writeFileSync("data/bigdata.json", JSON.stringify(bigquakes));
});

