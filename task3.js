/*Task 3
The authorities have a file of known vehicle registrations and the vehicle’s owner. Those vehicles with
standard registrations can be looked up in this file and a fine automatically sent out. A new file is created
by comparing the information from the average speed recording system with the file of registered
vehicles and their owners’ details. This new file should include the owner’s name and address details,
the registration of the vehicle and the average speed of the vehicle in the section of road.
Analyse the requirements for this system and design, develop, test and evaluate a program for creating
a file of details for vehicles exceeding the speed limit set for a section of road. You will need to create a
suitable file with test data, including standard registrations and vehicle owner information.
*/